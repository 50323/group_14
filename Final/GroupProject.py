# Import packages
import pandas as pd
from urllib.request import urlretrieve
import os
import plotly.express as px
import matplotlib.pyplot as plt
from statsmodels.tsa.arima.model import ARIMA
import pmdarima as pm
import numpy as np
import datetime as dt


class group_project_class:
    """
    Processes emissions data in each country and plots different comparative
    visualizations as well as an emissions forecast

    Methods
    ---------
    download_file(file_link)
        Downloads a file from URL into your hard drive
    available_countries()
        Extract unique countries in the dataset
    consumption_plot(country, normalize)
        Creates stacked area chart about a specific countries consumption
    compare_country_consumption(countries)
        Plot to compare electric consumption and emissions
    compare_country_gdp(countries)
        Plot to compare gdp
    gapminder_gdp_cons(year)
        Plot to compare GDP, energy consumption and population in a specific year
    gapminder_emi_cons(year)
        Same but with emissions instead of gdp
    forcasting(country, prediction_period)
        ARIMA forecasting plots of consumption and emissions
    """

    def __init__(self, df=None):

        self.df = pd.DataFrame()
        self.df = df

    def download_file(self, file_link: str):
        """
        Downloads a file from URL into your hard drive

        Parameters
        ------------
        file_link: str
            A string containing the link to the file you wish to download

        Returns
        ---------
        df: pandas data frame
            The prepared emissions data frame
        """
        path = "data.csv"

        # If file doesn't exist, download it. Else, print a warning message.
        if not os.path.exists(path):
            urlretrieve(file_link, filename=path)
        else:
            print("File already exists!")

        # Load into pandas data frame
        df = pd.read_csv(path)

        # Remove data before 1970
        df = df[df["year"] >= 1970]

        # Remove aggregated consumption columns
        agg_cols = [
            "renewables_consumption",
            "low_carbon_consumption",
            "fossil_fuel_consumption",
        ]
        df.drop(agg_cols, axis=1, inplace=True)

        # Add emissions column
        def emissions_mapping(
            country,
        ):  # maps the according emissions to the production modes
            e = (
                (
                    country["biofuel_consumption"] * 1450
                    + country["coal_consumption"] * 1000
                    + country["gas_consumption"] * 455
                    + country["hydro_consumption"] * 90
                    + country["nuclear_consumption"] * 5.5
                    + country["oil_consumption"] * 1200
                    + country["solar_consumption"] * 53
                    + country["wind_consumption"] * 14
                )
                / 1_000_000_000
                * 1_000_000
            )
            return e

        df["emissions"] = df.apply(emissions_mapping, axis=1)

        # Convert year to datetime and set as index while keeping original column
        df["year_index"] = pd.to_datetime(df["year"], format="%Y")
        df = df.set_index("year_index")

        self.df = df

    def available_countries(self):
        """
        Extract unique countries in the dataset

        Returns
        ---------
        list of unique countries
        """
        return self.df["country"].unique()

    def consumption_plot(self, country, normalize=False):
        """
        Creates stacked area chart about a specific countries consumption
        
        Parameters
        ------------
        country: str
            The country to be analyzed
        normalize: boolean
            Defines if the consumption will be normalized in relative terms
            before plotting

        Returns
        ---------
        Nothing but plots stacked area consumption visualization
        """
        if country not in self.available_countries():
            raise TypeError("ValueError")

        # Select country data
        df_country = self.df[self.df["country"] == country]

        # Get data frame of all consumption attributes except "total_consumption"
        df_consumption = df_country.filter(regex="_consumption", axis=1).copy()
        df_consumption = df_consumption.drop(columns="total_consumption")
        df_consumption = df_consumption.dropna()

        # Normalize if set to true
        if normalize:
            df_consumption = df_consumption.divide(df_consumption.sum(axis=1), axis=0)

        df_consumption.plot.area(figsize=(20, 7))

    def compare_country_consumption(self, countries: str):
        """
        Creates plot to compare electric consumption and emissions for defined
        countries. Energy consumption is shown as a continuous line and emissions
        as a dash line

        Parameters
        ------------
        countries: list of strings
            One or more countries we want to plot

        Returns
        ---------
        Nothing but plots graph with continuous and dash line for every country
        """

        if not isinstance(countries, list):  # if not list just string, create list
            countries = [countries]

        consumption_columns = [
            col
            for col in self.df.columns
            if "_consumption" in col
            and "primary_" not in col
            and "renewables" not in col
            and "fossil_fuel" not in col
            and "low_carbon" not in col
        ]  # creating list of all columns which contains consumption
        self.df["total_consumption"] = self.df[consumption_columns].sum(
            axis=1, skipna=True
        )  # sum horizontaly all consumption columns
        # filtered dataframe of received countries
        filter_df = self.df[self.df["country"].isin(countries)]

        # plotting with creating subplot
        ax = plt.subplots(figsize=(12, 5))[1]
        ax2 = ax.twinx()

        # for function creating every graph separetly
        for country in countries:
            # replace 0 (end of the graph was down)
            filter_df.replace({0: np.NaN}, inplace=True)
            # selecting one country I want to display
            filter_df_country = filter_df[filter_df["country"] == country]
            # first x and y axis
            fig = ax.plot(
                filter_df_country.index,
                filter_df_country["total_consumption"],
                label=country.title(),
            )
            # get the color I will asign to the second plot in emissions
            color = fig[0].get_color()
            # second plot y2 (emissions)
            ax2.plot(
                filter_df_country.index,
                filter_df_country["emissions"],
                "--",
                color=color,
                label=country.title(),
            )
        # some design
        ax.set_title("Energy consumption/Emissions per Country")
        ax.set_xlabel("Year")
        ax.set_ylabel("Energy consumption [TWh]")
        ax2.set_ylabel("EmissionsCO2[Tonnes]")
        ax.legend(title="Energy consumption", loc="upper left")
        ax2.legend(title="Emissions", loc="lower left")
        plt.show()

    def compare_country_gdp(self, countries: str):
        """
        Creates plot to compare gdp of defined countries over time

        Parameters
        ------------
        countries: list of strings
            One or more countries we want to plot

        Returns
        ---------
        Nothing but plots graph with country gdp comparison over time
        """

        if not isinstance(countries, list):  # if not list just string, create list
            countries = [countries]

        filter_df = self.df[self.df["country"].isin(countries)]
        fig = px.line(
            filter_df,
            x=filter_df.index,
            y="gdp",
            title="GDP comparation",
            color="country",
        )
        fig.show()

    def gapminder_gdp_cons(self, year: int):
        """
        Creates chart comparing country GDP, energy consumption and population
        in a specific year

        Parameters
        ------------
        year: int
            The year to be shown

        Returns
        ---------
        Nothing but plots gapminder visualization
        """
        # Check type
        if type(year) != int:
            raise TypeError("year is not an integer")

        # Get all consumption feature names
        consumption_feats = list(self.df.filter(regex="_consumption", axis=1).columns)

        # Create total consumption feature
        self.df["total_consumption"] = self.df[consumption_feats].sum(
            axis=1, skipna=True
        )

        # Filter year
        df_year = self.df.loc[pd.to_datetime(year, format="%Y")]

        # Remove "world" country and continents (identified by no iso_code)
        df_year = df_year[df_year["country"] != "World"]
        df_year = df_year[df_year["iso_code"].notna()]

        # Remove countries with missing values
        df_year = df_year[
            ["country", "gdp", "total_consumption", "population"]
        ].dropna()

        # Plot
        fig = px.scatter(
            df_year,
            x="gdp",
            y="total_consumption",
            size="population",
            color="country",
            hover_data=["country"],
            log_x=True,
            log_y=True,
        )
        fig.show()

    def gapminder_emi_cons(self, year: int):
        """
        Creates chart comparing country emissions, energy consumption and population
        in a specific year
        
        Parameters
        ------------
        year: int
            The year to be shown

        Returns
        ---------
        Nothing but plots gapminder visualization
        """
        # Check type
        if type(year) != int:
            raise TypeError("year is not an integer")

        # Get all consumption feature names
        consumption_feats = list(self.df.filter(regex="_consumption", axis=1).columns)

        # Create total consumption feature
        self.df["total_consumption"] = self.df[consumption_feats].sum(
            axis=1, skipna=True
        )

        # Filter year
        df_year = self.df.loc[pd.to_datetime(year, format="%Y")]

        # Remove "world" country and continents (identified by no iso_code)
        df_year = df_year[df_year["country"] != "World"]
        df_year = df_year[df_year["iso_code"].notna()]

        # Remove countries with missing values
        df_year = df_year[
            ["country", "emissions", "total_consumption", "population"]
        ].dropna()

        # Plot
        fig = px.scatter(
            df_year,
            x="emissions",
            y="total_consumption",
            size="population",
            color="country",
            hover_data=["country"],
            log_x=True,
            log_y=True,
        )
        fig.show()

    def forcasting(self, country: str, prediction_period: int):
        """
        Employs an algorithm from the ARIMA family to make a prediction method to
        forcast future development of total consumption and emission for the
        selected countries.
        
        This method will plot, side by side, two graphs: one with the predicted
        consumption and another with the predicted emissions
        
        Parameters
        ------------
        country: str
            The country to make a prediction on
        prediction period: int 
            The number of predicted periods must be >=1 and an int
            
        Returns
        ---------
        Nothing but plots the predicted consumption and predicted emission
        """

        # Check if country is in the list of countries, otherwise raise an error
        if country not in self.available_countries():
            raise TypeError("ValueError")

        # Check if prediction period is >=1
        if prediction_period < 1:
            raise TypeError("prediction period must be above or equal 1")

        # Get all consumption feature names
        consumption_feats = list(self.df.filter(regex="_consumption", axis=1).columns)

        # Create total consumption feature
        self.df["total_consumption"] = self.df[consumption_feats].sum(
            axis=1, skipna=True
        )

        # Select a specific country
        df_2 = self.df[["country", "emissions"]].dropna()
        df_2 = df_2[df_2["country"] == country]

        n_periods = prediction_period

        model = pm.auto_arima(
            df_2["emissions"],
            start_p=1,
            start_q=1,
            test="adf",  # use adftest to find optimal 'd'
            max_p=3,
            max_q=3,  # maximum p and q
            m=1,  # frequency of series
            d=None,  # let model determine 'd'
            seasonal=False,  # No Seasonality
            start_P=0,
            D=0,
            trace=True,
            error_action="ignore",
            suppress_warnings=True,
            stepwise=True,
        )
        # Forecast
        fc, confint = model.predict(n_periods=n_periods, return_conf_int=True)
        index_of_fc = pd.date_range(start="2019-01-01", periods=n_periods, freq="Y")

        # make series for plotting purpose
        fc_series = pd.Series(fc, index=index_of_fc)
        lower_series = pd.Series(confint[:, 0], index=index_of_fc)
        upper_series = pd.Series(confint[:, 1], index=index_of_fc)

        # Plot
        fig, ax = plt.subplots(figsize=(12, 5))
        df_2["emissions"].plot(ax=ax)
        fc_series.plot(ax=ax)
        plt.fill_between(
            lower_series.index, lower_series, upper_series, color="k", alpha=0.15
        )

        plt.title("Forecast Emissions")
        plt.show()

        # Select a specific country
        df_2 = self.df[["country", "total_consumption"]].dropna()
        df_2 = df_2[df_2["country"] == country]

        n_periods = prediction_period

        model = pm.auto_arima(
            df_2["total_consumption"].iloc[:-2],
            start_p=1,
            start_q=1,
            test="adf",  # use adftest to find optimal 'd'
            max_p=3,
            max_q=3,  # maximum p and q
            m=1,  # frequency of series
            d=None,  # let model determine 'd'
            seasonal=False,  # No Seasonality
            start_P=0,
            D=0,
            trace=True,
            error_action="ignore",
            suppress_warnings=True,
            stepwise=True,
        )
        # Forecast
        fc, confint = model.predict(n_periods=n_periods, return_conf_int=True)
        index_of_fc = pd.date_range(start="2019-01-01", periods=n_periods, freq="Y")

        # make series for plotting purpose
        fc_series = pd.Series(fc, index=index_of_fc)
        lower_series = pd.Series(confint[:, 0], index=index_of_fc)
        upper_series = pd.Series(confint[:, 1], index=index_of_fc)

        # Plot
        fig, ax = plt.subplots(figsize=(12, 5))
        df_2["total_consumption"].iloc[:-2].plot(ax=ax)
        fc_series.plot(ax=ax)
        plt.fill_between(
            lower_series.index, lower_series, upper_series, color="k", alpha=0.15
        )

        plt.title("Forecast Total Consumption")
        plt.show()

        plt.show(), plt.show()
