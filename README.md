## Group_14
Group Project AdPro Group 14 2022

## Description
We are four students persuing the Masters of Business Analytics at NOVA Business School, Lisboa. This project is part of our Advanced Programming Course. 

## Scenario
Our company is participating in a two-day hackathon promoted to study the energy mix of several countries. They send your group, the best team of Data Scientists in the company's roster. By analysing the energy mix of several countries, your company expects to contribute to the green transition by having a more savvy taskforce.

## Installation
To get started with the project, install all necessary libraries using the .ymal file available in the repository. Feel free to run the showcase "Showcase_1" or reuse the .py file. 

start running this function, to download the data and get started
    
    from GroupProject import group_project_class

    energy = group_project_class()
    energy.download_file(file_link='https://nyc3.digitaloceanspaces.com/owid-public/data/energy/owid-energy-data.csv')

## Data 

For this project, we will be using data from Our World in Data [Click here](https://ourworldindata.org). The dataset can be found [here] (https://github.com/owid/energy-data?country=).

## Authors and acknowledgment
Arabella Specker 48513@novasbe.pt

Fenja Indorf 50323@novasbe.pt

Fynn Oldenburg 46141@noasbe.pt

Vaclav Pasak 51089@novasbe.pt

## License
This Project is equipped with a MIT License. Please refer to this file for further details.

## Project status
This project is ended and will not be further updated. 